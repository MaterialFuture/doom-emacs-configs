;;; ~/.doom.d/+bindings.el -*- lexical-binding: t; -*-

;; Global Keys
(global-set-key [f8] 'neotree-toggle) ;

;; Leader key
(map! :leader
      :desc "M-x"                       "SPC" #'execute-extended-command ; Give your pinky a break
      :desc "Org agenda"                  "a" #'org-agenda
      :desc "Org agenda today"            "A" (lambda () (interactive) (org-agenda nil "a"))
      :desc "Org Journal"                 "J" (lambda () (interactive) (org-capture nil "j"))
      :desc "Org capture"                 "C" #'org-capture
      :desc "Expand region"               "<" #'er/expand-region
      :desc "Toggle popup window"         "`" #'+popup/toggle)

;; Prefix map
(map! :leader
      (:prefix ("b" . "buffer")         ; Emacs Buffer Bindings
        :desc "Previous buffer"         "TAB" #'previous-buffer
        :desc "Switch buffer"             "b" #'switch-to-buffer
        :desc "Delete buffer"             "d" #'kill-this-buffer ; SPC-b-k is the dedault, but both are still there
        :desc "Recent files"              "r" #'recentf-open-files
        :desc "Swith to home"             "h" #'+doom-dashboard/open)
      (:prefix ("g" . "git")            ; Magit bindings
        (:when (featurep! :tools magit) ; When Magic is available add these
          :desc "Magit pull"              "p" #'magit-pull
          :desc "Magit push"              "P" #'magit-push))
      (:prefix ("g m" . "Magit Remote") ; Git remote management
        (:when (featurep! :tools magit) ; When Magic is available add these
          :desc "Git Remotes"             "m" #'magit-remote
          :desc "Git Remote Remove"       "r" #'magit-remote-remove
          :desc "Git Remote Add"          "a" #'magit-remote-add))
      (:prefix ("r" . "crystal")        ; Crystal Lang bindings
        :desc "Crystal run"               "r" #'run-crystal
        :desc "Crystal tool format"       "p" #'crystal-tool-format
        :desc "Crystal run Spec All"      "P" #'crystal-spec-all)
      (:prefix ("o e" . "org extra")    ; Extra Org files to access
        :desc "Org Do What"               "w" #'find-file "~/.notes/org/what.org"))
